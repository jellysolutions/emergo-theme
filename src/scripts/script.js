$(document).ready(function () {
  $(".counter").counterUp({
    delay: 10,
    time: 1000,
  });
  var uri = $(location).attr("href");
  var hash = window.location.hash.slice(1);

  // var hash = url.substring(url.indexOf('#'))
  console.log(hash);
  $(".hero").css("background-position-y", 50 + "px");
  var viewWidth = $("body").width();
  var stickyNav = function () {
    var navH = $(".nav");
    var offset = navH.offset().top;
    var scrollTop = $(window).scrollTop();
    if (scrollTop > 70) {
      $(".nav").addClass("scrolled");
    } else {
      $(".nav").removeClass("scrolled");
    }
    if (viewWidth > 992) {
      $(window).on("scroll", function () {
        var scrollPos = scrollTop * -1 * 0.25;
        $(".hero").css("background-position-y", 50 + scrollPos + "px");
      });
    } else {
      $(".hero").css("background-position-y", 0 + "px");
    }
  };

  stickyNav();
  $(window).on("scroll resize", function () {
    stickyNav();
  });

  $("nav a").each(function (i, el) {
    if (
      !$(el).hasClass("nav-mobile__toggle") &&
      el.attributes.href.nodeValue == "#!"
    ) {
      $(el).addClass("not-finished-yet");
    }
  });

  $("nav a").each(function () {
    if ($(this).attr("href") == window.location.pathname) {
      $(this).addClass("selected-nav");
    }
  });

  $(".not-finished-yet").append(
    "<span class='under-construction' id='spantest'>W budowie :)</span>"
  );
  $(".not-finished-yet").on("click", function (e) {
    console.log($(e.target.children).hasClass("under-construction"));
    if ($(e.target.children).hasClass("under-construction")) {
      $(e.target.children).toggle();
      $(e.target.children).on("click", function () {
        $(e.target.children).hide();
      });
    }
  });
  const cookiePopup = document.querySelector("#cookie-popup");
  const cookiePopupAcceptBtn = document.querySelector("#cookie-popup-btn");
  const cookiesAcceptedLSkey = "cookiesAccepted";

  const acceptCookies = () => {
    cookiePopup.style.display = "none";
    localStorage.setItem(cookiesAcceptedLSkey, true);
  };

  const areCookiesAccepted = () => localStorage.getItem(cookiesAcceptedLSkey);

  const verifyCookiesPopup = () => {
    if (areCookiesAccepted()) {
      return;
    }
    cookiePopup.style.display = "block";
    cookiePopupAcceptBtn.addEventListener("click", acceptCookies);
  };

  verifyCookiesPopup();

  $(function () {
    $("nav .dropdown-btn").on("click", function (e) {
      e.preventDefault();
      console.log(e.target);
      var that = $(this);
      if ($(this).hasClass("opened-dropdown")) {
        $(this).parent().siblings(".menu-nav__dropdown").slideUp();
        $(this).removeClass("opened-dropdown");
      } else {
        $(this).addClass("opened-dropdown");
        $(this).parent().siblings(".menu-nav__dropdown").slideDown();
      }

      // $(".menu-nav__dropdown").not($(this).parent().siblings()).hide();

      e.stopPropagation();

      console.log($(this).parent().siblings());
    });
    $(".drop-nav").on("mouseenter", function (e) {
      e.preventDefault();
      $(this).children(".menu-nav__dropdown").slideDown();
    });
    $("nav ul li.menu-nav__item > a:only-child").on("mouseenter", function (e) {
      $(".menu-nav__dropdown").hide();
    });
    $(".menu-nav").on("mouseleave", function (e) {
      $(".menu-nav__dropdown").hide();
    });
    $(".drop-nav + ul").on("mouseleave", function (e) {
      $(".menu-nav__dropdown").hide();
    });

    $("#nav-toggle").on("click", function (e) {
      e.preventDefault();
      this.classList.toggle("active");
    });
    $("#nav-toggle").on("click", function () {
      $("nav ul.menu-nav").slideToggle();
    });

    var primaryPhoto = $("[data-photo='photo-1']");
    var secondaryPhoto = $("[data-photo='photo-2']");

    $(".accordion > li").on("click", function () {
      var img = $(this).attr("data-src");
      $("#switch-img").attr("src", img);
      console.log(img);
      console.log($("#switch-img"));
      console.log($(this));
      $(this).attr("data-photo");
      $("a").on("click", function (e) {
        e.stopPropagation();
      });
      if ($(this).hasClass("accordion__active")) {
        $(this)
          .removeClass("accordion__active")
          .find(".accordion__panel")
          .slideUp();
      } else {
        $(".accordion > .accordion__active .accordion__panel").slideUp();
        $(".accordion > .accordion__active").removeClass("accordion__active");
        $(this)
          .addClass("accordion__active")
          .find(".accordion__panel")
          .slideDown();
      }
      return false;
    });

    var counterTeaserL = $(".map-info");
    var winHeight = $(window).height();
    if (counterTeaserL.length) {
      var firEvent = false,
        objectPosTop = $(".map-info").offset().top;
      var elementViewInBottom = objectPosTop - winHeight;
      // $(window).on("scroll", function () {
      //   var currentPosition = $(document).scrollTop();
      //   if (currentPosition > elementViewInBottom && firEvent === false) {
      //     firEvent = true;
      //     animationCounter();
      //   }
      // });
    }

    function animationCounter() {
      $(".map-info__counter").each(function () {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 5000,
              easing: "swing",
              step: function (now) {
                $(this).text(Math.ceil(now));
              },
            }
          );
      });
    }
  });

  $(".point-box").on("mouseenter mouseleave", function (e) {
    $(this).parent().find(".point-info-box").toggle("fast");
  });
  $(".point-box").on("click", function (e) {
    $(this).parent().find(".point-info-box").show("fast");
    e.stopPropagation();
    $("html").on("click", function () {
      $(this).parent().find(".point-info-box").hide("fast");
    });
  });

  $(".btn--tertiary").on("mouseenter mouseleave", function (e) {
    $(e.target).parent().toggleClass("active-card");
  });

  $(".category-product-card__container").on("mouseenter", function (e) {
    $(this).addClass("hovered");
  });
  $(".category-product-card__container").on("mouseleave", function (e) {
    $(this).removeClass("hovered");
  });
  var contactPosition = $(".contact__service p");
  if (hash === $(".btn-selector").attr("data-form")) {
  }
  $(".btn-selector").each(function () {
    var id = $(this)[0].hash.slice(1);
    // console.log($(this)[0].hash.slice(1));
    if (id == hash) {
      console.log(hash);
      $(".btn-selector").removeClass("btn-secondary--main");
      $(".btn-selector").addClass("btn-secondary--light");
      $(this).addClass("btn-secondary--main");
      $(this).removeClass("btn-secondary--light");
      contactPosition.text($(this).attr("data-position"));
    }
  });
  console.log($(".btn-selector")[0]);
  $(".btn-selector").on("click", function (e) {
    var that = $(this);
    var dataBtn = that.attr("data-form");
    $(".btn-selector").removeClass("btn-secondary--main");
    $(".btn-selector").addClass("btn-secondary--light");
    $(this).addClass("btn-secondary--main");
    $(this).removeClass("btn-secondary--light");
    $("form").hide();
    $("form#" + dataBtn).show();

    contactPosition.text($(this).attr("data-position"));
  });
});

$(".point-wrapper-new").on("click mouseenter", function (e) {
  console.log($(this).children());
  var that = $(this);
  $(this).toggleClass("expand");
  that.children(".point-container").toggleClass("expand-container");
  that.children(".point-info-box-new").animate(
    {
      height: "100%",
      width: "100%",
      opacity: 1,
      // left: "+=50",
      // height: "toggle"
    },
    5000,
    function () {
      // Animation complete.
    }
  );
});

if ($(".brands-carousel") !== 0) {
  $(".brands-carousel").flexslider({
    animation: "slide",
    animationLoop: true,
    slideShow: true,
    itemWidth: 210,
    itemMargin: 0,
    minItems: 3,
    maxItems: 6,
    controlNav: true,
    touch: true,
    move: 1,
  });
}

if (
  $(".flexslider").length !== 0 ||
  $("#classic").length !== 0 ||
  $("#modern").length !== 0
) {
  $(".flexslider").flexslider({
    animation: "slide",
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a"),
  });
  $("#modern").flexslider({
    animation: "slide",
    customDirectionNav: $("#modern a.nav-arr"),
    controlNav: false,
    pauseOnHover: true,
  });

  var switcher = $(".switcher").children();
  var sliderModern = $("#modern");
  var sliderClassic = $("#classic");

  function toggleSwitch() {
    _this = $(this);

    var sliders = $("#sliders").children();
    console.log($("#sliders").find(".classic"));

    function toggleSlider() {
      sliderClassic.toggleClass("slider-active");
      sliderModern.toggleClass("slider-active");

      $("#classic").flexslider({
        animation: "slide",
        customDirectionNav: $("#classic a.nav-arr"),
        controlNav: false,
        pauseOnHover: true,
      });
    }

    if (
      !_this.hasClass("switch-selected") &&
      _this.siblings().hasClass("switch-selected")
    ) {
      _this.siblings().removeClass("switch-selected");
      _this.addClass("switch-selected");
      toggleSlider();
    }
  }
  switcher.on("click", toggleSwitch);
  var viewWidth2 = $("body").width();
  $(".point-expander").on("click mouseenter mouseleave", function (e) {
    if (viewWidth2 > 922) {
      if (!$(this).hasClass("expanded")) {
        $(".point-expander").removeClass("expanded");
        $(this).addClass("expanded");
      } else {
        // $(this).removeClass("expanded");
      }
      $(this).children(".point-content").css({ width: "auto" });
    } else {
      if (!$(this).hasClass("opened")) {
        $(".point-expander").removeClass("opened");
        $(this).addClass("opened");
      } else {
        $(this).removeClass("opened");
      }
    }
  });

  $(".point-expander").on("mouseout", function (e) {
    $(".point-expander").removeClass("opened");
  });

  $("html").on("click", function (e) {
    e.stopPropagation();
    var tag = e.target.tagName;
    if (tag == "IMG" || tag == "SECTION") {
      $(".point-expander").removeClass("opened");
      $(".point-expander").removeClass("expanded");
    }
  });
}
